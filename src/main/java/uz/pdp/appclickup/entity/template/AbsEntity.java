package uz.pdp.appclickup.entity.template;
// Nurkulov Nodirbek 4/9/2022  6:57 PM

import lombok.*;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@MappedSuperclass
public class AbsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private UUID id;

    @CreationTimestamp
    @Column(nullable = false,updatable = false)
    private LocalDateTime createdAt;

    @CreationTimestamp
    @Column(nullable = false,updatable = false)
    private LocalDateTime updatedAt;
}
