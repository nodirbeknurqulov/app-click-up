package uz.pdp.appclickup.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.pdp.appclickup.entity.template.AbsEntity;

import javax.persistence.Entity;

// Nurkulov Nodirbek 4/10/2022  7:29 AM
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "workspace_users")
public class WorkspaceUser extends AbsEntity {


}
