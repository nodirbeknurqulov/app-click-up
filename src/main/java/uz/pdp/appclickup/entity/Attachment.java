package uz.pdp.appclickup.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import uz.pdp.appclickup.entity.template.AbsEntity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

// Nurkulov Nodirbek 4/9/2022  6:58 PM
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "attachments")
public class Attachment extends AbsEntity {

    private String name;

    private String originalFileName;

    private long size;

    private String contentType;

    @JsonIgnore
    @OneToOne(mappedBy = "attachment", cascade = CascadeType.ALL)
    private AttachmentContent attachmentContent;

}
