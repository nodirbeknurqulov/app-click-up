package uz.pdp.appclickup.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import uz.pdp.appclickup.entity.template.AbsEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

// Nurkulov Nodirbek 4/10/2022  7:23 AM
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "icons")
public class Icon extends AbsEntity {

    @OneToOne
    private Attachment attachment;

    @Column(nullable = false)
    private String color;

    private char initialLetter;

    //icon
}
