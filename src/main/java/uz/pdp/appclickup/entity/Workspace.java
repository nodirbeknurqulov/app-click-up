package uz.pdp.appclickup.entity;
// Nurkulov Nodirbek 4/10/2022  6:46 AM

import lombok.*;
import uz.pdp.appclickup.entity.template.AbsEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "workspaces")
public class Workspace extends AbsEntity {

    @Column(nullable = false)
    private String fullName;

    @Column(nullable = false)
    private String color;

    @Column(nullable = false)
    private char initialLetter;

    @ManyToOne
    private User user;

    @OneToOne
    private Attachment attachment;
}
