package uz.pdp.appclickup.entity;

import lombok.*;
import uz.pdp.appclickup.entity.template.AbsEntity;

import javax.persistence.*;
import java.util.UUID;

// Nurkulov Nodirbek 4/9/2022  6:43 PM
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "users")
public class User extends AbsEntity {

    private String fullName;

    private String email;

    private String password;

    private String color;

    private Character initialLetter=Character.toUpperCase('N');

    @OneToOne
    private Attachment attachment;
}
