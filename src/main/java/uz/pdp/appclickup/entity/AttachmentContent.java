package uz.pdp.appclickup.entity;

import lombok.*;
import uz.pdp.appclickup.entity.template.AbsEntity;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToOne;

// Nurkulov Nodirbek 4/9/2022  7:37 PM
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity(name = "attachment_contents")
public class AttachmentContent extends AbsEntity {

    private byte[] data;

    @OneToOne(cascade = CascadeType.ALL)
    private Attachment attachment;
}
